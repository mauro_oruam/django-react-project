from django.shortcuts import render, get_object_or_404, redirect
from .models import Product, Purchase
from django.contrib.auth.models import User
from django.views.decorators.csrf import ensure_csrf_cookie
from .forms import PurchaseModelForm
from django.urls import reverse

from django.views.generic import(
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
 )
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    UserPassesTestMixin
)

class Product_create_view(LoginRequiredMixin, CreateView):
    model = Product
    fields = ['title', 'description', 'price', 'category']

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


def product_update_view(request, id=id):
    obj = get_object_or_404(Product, id=id)
    form = ProductForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
    context = {
        'form': form
    }
    return render(request, "products/product_create.html", context)


def product_list_view(request):
    queryset = Product.objects.all() # list of objects
    context = {
        "object_list": queryset
    }
    return render(request, "products/product_list.html", context)


def product_category_view(request, category):
    queryset = Product.objects.filter(category=category) # list of objects
    CATEGORY_CHOICES = Product.CATEGORY_CHOICES
    context = {
        "products": queryset,
        "CATEGORY_CHOICES": CATEGORY_CHOICES,
        "selected_category": category
    }

    return render(request, "products/product_category_view.html", context)

def product_detail_view(request, id):
    obj = get_object_or_404(Product, id=id)
    context = {
        "object": obj
    }
    return render(request, "products/product_detail.html", context)


def product_delete_view(request, id):
    obj = get_object_or_404(Product, id=id)
    if request.method == "POST":
        obj.delete()
        return redirect('../../')
    context = {
        "object": obj
    }
    return render(request, "products/product_delete.html", context)


class Purchase_list_view(ListView):
    model = Purchase
    template_name = 'products/purchase_list.html'
    context_object_name = 'purchases'
    ordering = ['-date_purchased']
    paginate_by = 5

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        print(user)
        return Purchase.objects.filter(user=user).order_by('-date_purchased')


class Purchase_detail_view(DetailView):
    model = Purchase

class Purchase_create_view(LoginRequiredMixin, CreateView):
    model = Purchase
    form = PurchaseModelForm
    template_name= 'products/purchase_form.html'
    fields = ['pay_mode', 'amount']

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.product = Product.objects.get(pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('products:purchases',kwargs={'username': self.request.user} )
