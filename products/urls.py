from django.urls import path
from .views import (
    Product_create_view,
    product_detail_view,
    product_delete_view,
    product_list_view,
    product_update_view,
    product_category_view,
    Purchase_detail_view,
    Purchase_create_view,
    Purchase_list_view,

)

app_name = 'products'
urlpatterns = [
    path('products/', product_list_view, name='product-list'),
    path('products/create/', Product_create_view.as_view(), name='product-create'),
    path('products/<int:id>/', product_detail_view, name='product-detail'),
    path('products/<int:id>/update/', product_update_view, name='product-update'),
    path('products/<int:id>/delete/', product_delete_view, name='product-delete'),
    path('products/<str:category>/', product_category_view, name='product-category'),
    path('purchases/<int:pk>/', Purchase_detail_view.as_view(), name='purchase-detail'),
    path('purchases/<str:username>', Purchase_list_view.as_view(), name='purchases'),
    path('products/<int:pk>/purchase/', Purchase_create_view.as_view(), name='purchase-create'),

]
