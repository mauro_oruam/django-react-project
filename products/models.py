from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.models import User



# Create your models here.
class Product(models.Model):
    CATEGORY_CHOICES = (
    ('services', 'SERVICES'),
    ('garden', 'GARDEN'),
    ('electronic', 'ELECTRONIC'),
    ('clothes', 'CLOTHES'),
    ('hobbies', 'HOBBIES'),
    )
    title       = models.CharField(max_length=120) # max_length = required
    description = models.TextField(blank=True, null=True)
    price       = models.DecimalField(decimal_places=2, max_digits=10000)
    featured    = models.BooleanField(default=False) # null=True, default=True
    author      = models.ForeignKey(User, on_delete=models.CASCADE, default="1")
    date_posted = models.DateTimeField(default=timezone.now)
    category    = models.CharField(max_length=120, choices=CATEGORY_CHOICES, default='services')

    def get_absolute_url(self):
        return reverse('products:product-detail', kwargs={'id': self.id})



    # Para que se vea el título del post al imprimirlo en el shell
    def __str__(self):
        return self.title


# Create your models here.
class Purchase(models.Model):
    PAYMENT_CHOICES = (
    ('cash', 'CASH'),
    ('debit card', 'DEBIT CARD'),
    ('credit card', 'CREDIT CARD')
    )
    product        = models.ForeignKey(Product, on_delete=models.CASCADE)
    user           = models.ForeignKey(User, on_delete=models.CASCADE)
    amount         = models.IntegerField(default=1)
    date_purchased = models.DateTimeField(default=timezone.now)
    pay_mode       = models.CharField(max_length=30, choices=PAYMENT_CHOICES, default='cash')
    state          = models.CharField(max_length=30, default='closed')

    def get_absolute_url(self):
        return reverse('products:purchase-detail', kwargs={'pk': self.pk})



    # Para que se vea el título del post al imprimirlo en el shell
    def __str__(self):
        return self.product.title
