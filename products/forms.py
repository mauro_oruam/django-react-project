from django import forms
from .models import Purchase


class PurchaseModelForm(forms.ModelForm):
    class Meta:
        model = Purchase
        exclude =(
            'user',
            'product',
        )
