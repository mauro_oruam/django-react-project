from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User
from products.models import Product
from django.views.generic import(
    ListView,
 )

def home(request):
    context = {
        'products': Product.objects.all()
    }
    return render(request, 'ecommerce/home.html', context)

def about(request):
    return render(request, 'ecommerce/about.html', {'title':'about'})
