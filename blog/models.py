from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.

class Post (models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    # Pasamos la función now sin paréntesis para pasarla como valor
    date_posted = models.DateTimeField(default=timezone.now)
    # Si el usuario se elimina se borrarán sus posts
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    # Para que se vea el título del post al imprimirlo en el shell
    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'pk': self.pk})
